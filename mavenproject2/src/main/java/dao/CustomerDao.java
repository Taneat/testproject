/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.searchcustomer.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Taneat
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer (name,tel,point) VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getPoint());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, tel FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            while (resul.next()) {
                int id = resul.getInt("id");
                String name = resul.getString("name");
                String tel = resul.getString("tel");
                int point = resul.getInt("point");
                Customer customer = new Customer(id, name, tel,point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    public Customer get(String tel) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, tel, point FROM customer WHERE tel=" + tel;
            Statement stmt = conn.createStatement();
            ResultSet resul = stmt.executeQuery(sql);
            if (resul.next()) {
                int cid = resul.getInt("id");
                String name = resul.getString("name");
                String tell = resul.getString("tel");
                int point = resul.getInt("point");
                Customer customer = new Customer(cid, name, tell,point);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;    }

    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "Anna", "0912223333",50));
        System.out.println("id: " + id);
        Customer lastCustomer = dao.get(id);
        System.out.println("last customer: "+lastCustomer);
        lastCustomer.setTel("0945555555");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer: "+updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete customer: "+deleteCustomer);
    }

    @Override
    public Customer get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
